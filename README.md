# libranim

A collection of open source / free animations for use in game engines. Animations are 3D unless specified.

As of now animations sets are:

cube-guy - A character and set of FPS animations converted from Tesseract / Cube 2. Licensed CC0

jack - The Jack sample character from o3de. Licensed MIT.

BURT - The BURT sample character from o3de. Licensed MIT.